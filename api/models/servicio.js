/* eslint-disable indent */
/**
 * @swagger
 * definitions:
 *   Servicio:
 *     type: object
 *     properties:
 *       id:
 *         type: integer
 *       id_empleado:
 *         type: integer
 *       id_tiposervicio:
 *         type: integer
 *       valor:
 *         type: integer
 *       fecha:
 *         type: date-time
 *       required:
 *         - id_empleado
 *         - id_tiposervicio
 *         - valor
 *         - fecha
 */

module.exports = (sequelize, type) => sequelize.define('servicio', {
    id: {
        type: type.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    id_empleado: {
        type: type.INTEGER,
        allowNull: false,
    },
    id_tiposervicio: {
        type: type.INTEGER,
        allowNull: false,
    },
    valor: {
        type: type.INTEGER,
        allowNull: false,
    },
    fecha: {
        type: type.DATE,
        allowNull: false,
    },
});