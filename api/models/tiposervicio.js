/* eslint-disable indent */
/**
 * @swagger
 * definitions:
 *   TipoServicio:
 *     type: object
 *     properties:
 *       id:
 *         type: integer
 *       nombre:
 *         type: string
 *       required:
 *         - nombre
 */

module.exports = (sequelize, type) => sequelize.define('tiposervicio', {
    id: {
        type: type.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    nombre: {
        type: type.STRING,
        allowNull: false,
    },
});