/* eslint-disable indent */
/**
 * @swagger
 * definitions:
 *   Empleado:
 *     type: object
 *     properties:
 *       id:
 *         type: integer
 *       nombre:
 *         type: string
 *       codigo:
 *         type: string
 *       cargo:
 *         type: string
 *       porcentaje:
 *         type: double
 *       required:
 *         - nombre
 *         - codigo
 *         - cargo
 *         - porcentaje
 */

module.exports = (sequelize, type) => sequelize.define('empleado', {
    id: {
        type: type.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    nombre: {
        type: type.STRING,
        allowNull: false,
    },
    codigo: {
        type: type.STRING,
        allowNull: false,
    },
    cargo: {
        type: type.STRING,
        allowNull: false,
    },
    porcentaje: {
        type: type.FLOAT,
        allowNull: false,
    },
    
});