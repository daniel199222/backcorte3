/* eslint-disable no-console */
import passport from 'passport';
import Models from '../../sequelize';

var Empleado = Models.Empleado;

/**
 * @swagger
 * /deleteEmpleado:
 *   delete:
 *     tags:
 *       - Empleados
 *     name: Delete Empleado
 *     summary: Delete Empleado
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: query
 *         schema:
 *           $ref: '#/definitions/Empleado'
 *           type: integer
 *         required:
 *           - id
 *     responses:
 *       '200':
 *         description: Empleado deleted from db
 *       '403':
 *         description: Authentication error
 *       '404':
 *         description: No Empleado in db with that name
 *       '500':
 *         description: Problem communicating with db
 */

module.exports = (app) => {
  app.delete('/deleteEmpleado', (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
      if (err) {
        console.error(err);
      }
      if (info !== undefined) {
        console.error(info.message);
        res.status(403).send(info.message);
      } else {
        Empleado.destroy({
          where: {
            id: req.query.id,
          },
        })
          .then((empleadoInfo) => {
            if (empleadoInfo === 1) {
              console.log('Empleado deleted from db');
              res.status(200).send('Empleado deleted from db');
            } else {
              console.error('Empleado not found in db');
              res.status(404).send('No Empleado with that id to delete');
            }
          })
          .catch((error) => {
            console.error('problem communicating with db');
            res.status(500).send(error);
          });
      }
    })(req, res, next);
  });
};
