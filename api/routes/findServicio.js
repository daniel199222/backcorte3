/* eslint-disable no-console */
import passport from 'passport';
import Models from '../../sequelize';

var Servicio = Models.Servicio;
/**
 * @swagger
 * /findServicio:
 *   get:
 *     tags:
 *       - Servicios
 *     name: Find servicio
 *     summary: Finds a servicio
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: id
 *         schema:
 *           type: integer
 *         required:
 *           - id
 *     responses:
 *       '200':
 *         description: A single servicio object
 *         schema:
 *           $ref: '#/definitions/Servicio'
 *       '401':
 *         description: No auth token / no user found in db with that name
 *       '403':
 *         description: JWT token and username from client don't match
 */

module.exports = (app) => {
    app.get('/findServicio', (req, res, next) => {
        passport.authenticate('jwt', { session: false }, (err, user, info) => {
            if (err) {
                console.log(err);
            }
            if (info !== undefined) {
                console.log(info.message);
                res.status(401).send(info.message);
            } else {
                Servicio.findOne({
                    where: {
                        id: req.query.id,
                    },
                }).then((servicioInfo) => {
                    if (servicioInfo != null) {
                        console.log('Servicio found in db from findservicio');
                        res.status(200).send({
                            auth: true,
                            id:servicioInfo.id,
                            id_empleado: servicioInfo.id_empleado,
                            id_tiposervicio: servicioInfo.id_tiposervicio,
                            valor: servicioInfo.valor,
                            fecha: servicioInfo.fecha,
                            message: 'servicio found in db',
                        });
                    } else {
                        console.error('no servicio exists in db with that username');
                        res.status(401).send('no servicio exists in db with that username');
                    }
                });
            }
        })(req, res, next);
    });
};
