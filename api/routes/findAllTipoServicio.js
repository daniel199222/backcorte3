/* eslint-disable no-console */
import passport from 'passport';
import Models from '../../sequelize';

var TipoServicio = Models.TipoServicio;
/**
 * @swagger
 * /findTipoServicio:
 *   get:
 *     tags:
 *       - TipoServicios
 *     name: Find tipo servicio
 *     summary: Finds a tipo servicio
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: A single tipo servicio object
 *         schema:
 *           $ref: '#/definitions/TipoServicio'
 *       '401':
 *         description: No auth token / no user found in db with that name
 *       '403':
 *         description: JWT token and username from client don't match
 */

module.exports = (app) => {
    app.get('/findAllTipoServicio', (req, res, next) => {
        passport.authenticate('jwt', { session: false }, (err, user, info) => {
            if (err) {
                console.log(err);
            }
            if (info !== undefined) {
                console.log(info.message);
                res.status(401).send(info.message);
            } else {
                TipoServicio.findAll()
                .then((tiposervicioInfo) => {
                    if (tiposervicioInfo != null) {
                        console.log('Tipos de servicios found in db from findAllServicio');
                        res.status(200).send({
                            data: tiposervicioInfo,
                            message: 'tipo servicio found in db',
                        });
                    } else {
                        console.error('no tipo servicio exists in db with that username');
                        res.status(401).send('no tipo servicio exists in db with that username');
                    }
                });
            }
        })(req, res, next);
    });
};
