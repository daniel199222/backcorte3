/* eslint-disable arrow-parens */
/* eslint-disable no-console */
import passport from 'passport';
import Models from '../../sequelize';

var Servicio = Models.Servicio;

/**
 * @swagger
 * /registerServicio:
 *   post:
 *     tags:
 *       - Servicios
 *     name: Register
 *     summary: Register a new servicio
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Servicio'
 *           type: object
 *           properties:
 *             id_empleado:
 *               type: integer
 *             id_tiposervicio:
 *               type: integer
 *             valor:
 *               type: integer
 *             fecha:
 *               type: "string"
 *               format: "date-time"
 *         required:
 *           - id_empleado
 *           - id_tiposervicio
 *           - valor
 *           - fecha
 *     responses:
 *       '200':
 *         description: Servicio created
 *       '403':
 *         description: Username or email already taken
 */

module.exports = app => {
  app.post('/registerServicio', (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
      if (err) {
        console.error(err);
      }
      if (info !== undefined) {
        console.error(info.message);
        res.status(403).send(info.message);
      } else {

        // eslint-disable-next-line no-unused-vars
        const data = {
          id_empleado: req.body.id_empleado,
          id_tiposervicio: req.body.id_tiposervicio,
          valor: req.body.valor,
          fecha: req.body.fecha,
        };

        Servicio
          .create({
            id_empleado: data.id_empleado,
            id_tiposervicio: data.id_tiposervicio,
            valor: data.valor,
            fecha: data.fecha,
          })
          .then((servicio) => {
            console.log('servicio created in db');
            res.status(200).send({ message: 'servicio created' });
          });
      }
    })(req, res, next);
  });
};
