/* eslint-disable no-console */
import passport from 'passport';
import Models from '../../sequelize';
//import { Sequelize } from 'sequelize/types';


var Servicio = Models.Servicio;
var Empleado = Models.Empleado;
/**
 * @swagger
 * /findPaymentEmpleado:
 *   get:
 *     tags:
 *       - Servicios
 *     name: FindPaymentEmpleado
 *     summary: Logs in a user
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Servicio'
 *           type: object
 *           properties:
 *             idempleado:
 *               type: integer
 *             
 *         required:
 *           - idempleado
 *     responses:
 *       '200':
 *         description: User found and logged in successfully
 *       '401':
 *         description: Bad username, not found in db
 *       '403':
 *         description: Username and password don't match
 */

module.exports = (app) => {
    //req -> la petición (contiene lo que tiene la petición) res -> reponse (respuesta) -> next: continuar en el ciclo
    app.get('/findPaymentEmpleado', (req, res, next) => {
        passport.authenticate('jwt', { session: false }, (err, user, info) => {
            if (err) {
                console.log(err);
            }
            if (info !== undefined) {
                console.log(info.message);
                res.status(401).send(info.message);
            } else {
                Servicio.sum('valor',{
                    where: { id_empleado: req.query.idempleado  }
                }).then(suma => {
                    if (suma != null) {
                        Empleado.findOne({
                            attributes: ['porcentaje'],
                            where: { id: req.query.idempleado  }
                        }).then(empleado =>{
                            if (empleado.porcentaje != null) {
                                console.log(suma,empleado.porcentaje);
                                let resultado=(suma*empleado.porcentaje)/100;
                                console.log('Query exitosa');
                                res.status(200).send({
                                    resultado:resultado
                                });
                            }
                        });
                    } else {
                        console.error('no se encontraron servicios para el empleado');
                        res.status(401).send('no servicio exists in db with that id_empleado');
                    }
                });
            }
        })(req, res, next);
    });
};
