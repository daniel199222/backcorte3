/* eslint-disable no-console */
import passport from 'passport';
import Models from '../../sequelize';

var Empleado = Models.Empleado;

/**
 * @swagger
 * /updateEmpleado:
 *   put:
 *     tags:
 *       - Empleados
 *     name: Update tiposervicio
 *     summary: Update empleado
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Empleado'
 *           type: object
 *           properties:
 *             id:
 *              type: integer
 *             nombre:
 *               type: string
 *             codigo:
 *               type: string
 *             cargo:
 *               type: string
 *             porcentaje:
 *               type: number
 *         required:
 *           - id
 *     responses:
 *       '200':
 *         description: Empleado info updated
 *       '403':
 *         description: No authorization / user not found
 */

module.exports = (app) => {
  app.put('/updateEmpleado', (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
      if (err) {
        console.error(err);
      }
      if (info !== undefined) {
        console.error(info.message);
        res.status(403).send(info.message);
      } else {
        Empleado.findOne({
          where: {
            id: req.body.id,
          },
        }).then((empleadoInfo) => {
          if (empleadoInfo != null) {
            console.log('Empleado found in db');
            empleadoInfo
              .update({
                nombre: req.body.nombre,
                codigo: req.body.codigo,
                cargo: req.body.cargo,
                porcentaje: req.body.porcentaje,
              })
              .then(() => {
                console.log('Empleado updated');
                res.status(200).send({ auth: true, message: 'Empleado updated' });
              });
          } else {
            console.error('no Empleado exists in db to update');
            res.status(401).send('no Empleado exists in db to update');
          }
        });
      }
    })(req, res, next);
  });
};
