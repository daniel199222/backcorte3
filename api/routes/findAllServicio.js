/* eslint-disable no-console */
import passport from 'passport';
import Models from '../../sequelize';

var Servicio = Models.Servicio;
/**
 * @swagger
 * /findAllServicio:
 *   get:
 *     tags:
 *       - Servicios
 *     name: Find All servicio
 *     summary: Finds all servicio in DB
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: An array of servicios
 *         schema:
 *           $ref: '#/definitions/Servicio'
 *       '401':
 *         description: No auth token / no user found in db with that name
 *       '403':
 *         description: JWT token and username from client don't match
 */

module.exports = (app) => {
    app.get('/findAllServicio', (req, res, next) => {
        passport.authenticate('jwt', { session: false }, (err, user, info) => {
            if (err) {
                console.log(err);
            }
            if (info !== undefined) {
                console.log(info.message);
                res.status(401).send(info.message);
            } else {
                Servicio.findAll ()
                .then((servicioInfo) => {
                    if (servicioInfo != null) {
                        console.log('Servicio found in db from findservicio');
                        res.status(200).send({
                            data: servicioInfo,
                            message: 'servicios found in db',
                        });
                    } else {
                        console.error('no servicio exists in db with that username');
                        res.status(401).send('no servicio exists in db with that username');
                    }
                });
            }
        })(req, res, next);
    });
};
