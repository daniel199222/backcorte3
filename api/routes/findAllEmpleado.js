/* eslint-disable no-console */
import passport from 'passport';
import Models from '../../sequelize';

var Empleado = Models.Empleado;
/**
 * @swagger
 * /findAllEmpleado:
 *   get:
 *     tags:
 *       - Empleados
 *     name: Find All empleado
 *     summary: Finds all empleado in DB
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: An array of empleados
 *         schema:
 *           $ref: '#/definitions/Empleados'
 *       '401':
 *         description: No auth token / no user found in db with that name
 *       '403':
 *         description: JWT token and username from client don't match
 */

module.exports = (app) => {
    app.get('/findAllEmpleado', (req, res, next) => {
        passport.authenticate('jwt', { session: false }, (err, user, info) => {
            if (err) {
                console.log(err);
            }
            if (info !== undefined) {
                console.log(info.message);
                res.status(401).send(info.message);
            } else {
                Empleado.findAll ()
                .then((empleadoInfo) => {
                    if (empleadoInfo != null) {
                        console.log('empleado found in db from findempleado');
                        res.status(200).send({
                            data: empleadoInfo,
                            message: 'empleados found in db',
                        });
                    } else {
                        console.error('no empleado exists in db with that username');
                        res.status(401).send('no empleado exists in db with that username');
                    }
                });
            }
        })(req, res, next);
    });
};
