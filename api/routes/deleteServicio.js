/* eslint-disable no-console */
import passport from 'passport';
import Models from '../../sequelize';

var Servicio = Models.Servicio;

/**
 * @swagger
 * /deleteServicio:
 *   delete:
 *     tags:
 *       - Servicios
 *     name: Delete servicio
 *     summary: Delete servicio
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: query
 *         schema:
 *           $ref: '#/definitions/Servicio'
 *           type: integer
 *         required:
 *           - id
 *     responses:
 *       '200':
 *         description: Servicio deleted from db
 *       '403':
 *         description: Authentication error
 *       '404':
 *         description: No servicio in db with that id
 *       '500':
 *         description: Problem communicating with db
 */

module.exports = (app) => {
  app.delete('/deleteServicio', (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
      if (err) {
        console.error(err);
      }
      if (info !== undefined) {
        console.error(info.message);
        res.status(403).send(info.message);
      } else {
        Servicio.destroy({
          where: {
            id: req.query.id,
          },
        })
          .then((servicioInfo) => {
            if (servicioInfo === 1) {
              console.log('servicio deleted from db');
              res.status(200).send('servicio deleted from db');
            } else {
              console.error('user not found in db');
              res.status(404).send('no servicio with that id to delete');
            }
          })
          .catch((error) => {
            console.error('problem communicating with db');
            res.status(500).send(error);
          });
      }
    })(req, res, next);
  });
};
