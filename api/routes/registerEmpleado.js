/* eslint-disable arrow-parens */
/* eslint-disable no-console */
import passport from 'passport';
import Models from '../../sequelize';

var Empleado = Models.Empleado;

/**
 * @swagger
 * /registerEmpleado:
 *   post:
 *     tags:
 *       - Empleados
 *     name: Register
 *     summary: Register a new empleado
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Empleado'
 *           type: object
 *           properties:
 *             nombre:
 *               type: string
 *             codigo:
 *               type: string
 *             cargo:
 *               type: string
 *             porcentaje:
 *               type: number
 *         required:
 *         - nombre
 *         - codigo
 *         - cargo
 *         - porcentaje
 *     responses:
 *       '200':
 *         description: Empleado created
 *       '403':
 *         description: Empleado or codigo already taken
 */

module.exports = app => {
  app.post('/registerEmpleado', (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
      if (err) {
        console.error(err);
      }
      if (info !== undefined) {
        console.error(info.message);
        res.status(403).send(info.message);
      } else {
        // eslint-disable-next-line no-unused-vars        
        Empleado
          .create({
            nombre: req.body.nombre,
            codigo: req.body.codigo,
            cargo: req.body.cargo,
            porcentaje: req.body.porcentaje,
          })
          .then((empleado) => {
            console.log('empleado created in db');
            res.status(200).send({ message: 'empleado created' });
          });
      }
    })(req, res, next);
  });
};
