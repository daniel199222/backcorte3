/* eslint-disable arrow-parens */
/* eslint-disable no-console */
import passport from 'passport';
import Models from '../../sequelize';

var TipoServicio = Models.TipoServicio;

/**
 * @swagger
 * /registerTipoServicio:
 *   post:
 *     tags:
 *       - TipoServicios
 *     name: Register
 *     summary: Register a new tipo servicio
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/TipoServicio'
 *           type: object
 *           properties:
 *             nombre:
 *               type: string
 *         required:
 *           - nombre
 *     responses:
 *       '200':
 *         description: tipo servicio created
 *       '403':
 *         description: Username or email already taken
 */

module.exports = app => {
  app.post('/registerTipoServicio', (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
      if (err) {
        console.error(err);
      }
      if (info !== undefined) {
        console.error(info.message);
        res.status(403).send(info.message);
      } else {
        // eslint-disable-next-line no-unused-vars        
        TipoServicio
          .create({
            nombre: req.body.nombre,
          })
          .then(() => {
            console.log('tipo de servicio created in db');
            res.status(200).send({ message: 'tipo de servicio created' });
          });
      }
    })(req, res, next);
  });
};
