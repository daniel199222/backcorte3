/* eslint-disable no-console */
import passport from 'passport';
import Models from '../../sequelize';

var TipoServicio = Models.TipoServicio;

/**
 * @swagger
 * /deleteTipoServicio:
 *   delete:
 *     tags:
 *       - TipoServicios
 *     name: Delete Tipo servicio
 *     summary: Delete Tipo servicio
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: query
 *         schema:
 *           $ref: '#/definitions/TipoServicio'
 *           type: integer
 *         required:
 *           - id
 *     responses:
 *       '200':
 *         description: Tipo Servicio deleted from db
 *       '403':
 *         description: Authentication error
 *       '404':
 *         description: No tipo de servicio in db with that name
 *       '500':
 *         description: Problem communicating with db
 */

module.exports = (app) => {
  app.delete('/deleteTipoServicio', (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
      if (err) {
        console.error(err);
      }
      if (info !== undefined) {
        console.error(info.message);
        res.status(403).send(info.message);
      } else {
        TipoServicio.destroy({
          where: {
            id: req.query.id,
          },
        })
          .then((tipoServicioInfo) => {
            if (tipoServicioInfo === 1) {
              console.log('tipo de servicio deleted from db');
              res.status(200).send('tipo de servicio deleted from db');
            } else {
              console.error('tipo de servicio not found in db');
              res.status(404).send('no tipo de servicio with that id to delete');
            }
          })
          .catch((error) => {
            console.error('problem communicating with db');
            res.status(500).send(error);
          });
      }
    })(req, res, next);
  });
};
