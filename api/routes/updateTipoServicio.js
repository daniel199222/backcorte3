/* eslint-disable no-console */
import passport from 'passport';
import Models from '../../sequelize';

var TipoServicio = Models.TipoServicio;

/**
 * @swagger
 * /updateTipoServicio:
 *   put:
 *     tags:
 *       - TipoServicios
 *     name: Update tiposervicio
 *     summary: Update tipo servicio
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/TipoServicio'
 *           type: object
 *           properties:
 *             id:
 *              type: integer
 *             nombre:
 *               type: string
 *         required:
 *           - id
 *     responses:
 *       '200':
 *         description: Tipo de Servicio info updated
 *       '403':
 *         description: No authorization / user not found
 */

module.exports = (app) => {
  app.put('/updateTipoServicio', (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
      if (err) {
        console.error(err);
      }
      if (info !== undefined) {
        console.error(info.message);
        res.status(403).send(info.message);
      } else {
        TipoServicio.findOne({
          where: {
            id: req.body.id,
          },
        }).then((tiposervicioInfo) => {
          if (tiposervicioInfo != null) {
            console.log('tipo de servicio found in db');
            tiposervicioInfo
              .update({
                nombre: req.body.nombre,
              })
              .then(() => {
                console.log('tipo servicio updated');
                res.status(200).send({ auth: true, message: 'tipo servicio updated' });
              });
          } else {
            console.error('no tipo de servicio exists in db to update');
            res.status(401).send('no tipo de servicio exists in db to update');
          }
        });
      }
    })(req, res, next);
  });
};
