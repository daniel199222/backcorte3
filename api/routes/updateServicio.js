/* eslint-disable no-console */
import passport from 'passport';
import Models from '../../sequelize';

var Servicio = Models.Servicio;

/**
 * @swagger
 * /updateServicio:
 *   put:
 *     tags:
 *       - Servicios
 *     name: Update servicio
 *     summary: Update servicio info
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Servicio'
 *           type: object
 *           properties:
 *             id:
 *              type: integer
 *             id_empleado:
 *               type: integer
 *             id_tiposervicio:
 *               type: integer
 *             valor:
 *               type: integer
 *             fecha:
 *               type: date-time
 *         required:
 *           - id
 *     responses:
 *       '200':
 *         description: servicio info updated
 *       '403':
 *         description: No authorization / user not found
 */

module.exports = (app) => {
  app.put('/updateServicio', (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
      if (err) {
        console.error(err);
      }
      if (info !== undefined) {
        console.error(info.message);
        res.status(403).send(info.message);
      } else {
        Servicio.findOne({
          where: {
            id: req.body.id,
          },
        }).then((servicioInfo) => {
          if (servicioInfo != null) {
            console.log('servicio found in db');
            servicioInfo
              .update({
                id_empleado: req.body.id_empleado,
                id_tiposervicio: req.body.id_tiposervicio,
                valor: req.body.valor,
                fecha: req.body.fecha,
              })
              .then(() => {
                console.log('servicio updated');
                res.status(200).send({ auth: true, message: 'servicio updated' });
              });
          } else {
            console.error('no servicio exists in db to update');
            res.status(401).send('no servicio exists in db to update');
          }
        });
      }
    })(req, res, next);
  });
};
