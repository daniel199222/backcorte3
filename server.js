import express from 'express';
import Cors from 'cors';
import bodyParser from 'body-parser';
import logger from 'morgan';
import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import passport from 'passport';
import helmet from 'helmet';

const app = express();

const API_PORT = process.env.API_PORT || 3000;

const swaggerDefinition = {
  info: {
    title: 'Peluqueria Swagger API',
    version: '1.0.0',
    description: 'Endpoints utilizados por la App Movil',
  },
  host: 'localhost:3003',
  basePath: '/',
  securityDefinitions: {
    bearerAuth: {
      type: 'apiKey',
      name: 'Authorization',
      scheme: 'bearer',
      in: 'header',
    },
  },
};

const options = {
  swaggerDefinition,
  apis: ['./api/routes/*.js'],
};

const swaggerSpec = swaggerJSDoc(options);

app.get('/swagger.json', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

require('./api/config/passport');

const whitelist = [
  'http://localhost:3031',
  'http://localhost:3000',
  'http://localhost:3003',
];
const corsOptions = {
  origin: (origin, callback) => {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
  optionsSuccessStatus: 200,
};

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use(Cors(corsOptions));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger('dev'));
app.use(helmet());
app.use(passport.initialize());

// Usuario
require('./api/routes/loginUser')(app);
require('./api/routes/registerUser')(app);
require('./api/routes/forgotPassword')(app);
require('./api/routes/resetPassword')(app);
require('./api/routes/updatePassword')(app);
require('./api/routes/updatePasswordViaEmail')(app);
require('./api/routes/findUsers')(app);
require('./api/routes/deleteUser')(app);
require('./api/routes/updateUser')(app);


// Servicio
require('./api/routes/registerServicio')(app);
require('./api/routes/findServicio')(app);
require('./api/routes/findAllServicio')(app);
require('./api/routes/updateServicio')(app);
require('./api/routes/deleteServicio')(app);
require('./api/routes/findPaymentEmpleado')(app);

// Tipo Servicio
require('./api/routes/registerTipoServicio')(app);
require('./api/routes/findTipoServicio')(app);
require('./api/routes/findAllTipoServicio')(app);
require('./api/routes/updateTipoServicio')(app);
require('./api/routes/deleteTipoServicio')(app);

// Empleado
require('./api/routes/registerEmpleado')(app);
require('./api/routes/findAllEmpleado')(app);
require('./api/routes/updateEmpleado')(app);
require('./api/routes/deleteEmpleado')(app);

// eslint-disable-next-line no-console
app.listen(API_PORT, () => console.log(`Listening on port ${API_PORT}`));

module.exports = app;
