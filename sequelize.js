import Sequelize from 'sequelize';
import UserModel from './api/models/user';
import ServicioModel from './api/models/servicio';
import TipoServicioModel from './api/models/tiposervicio';
import EmpleadoModel from './api/models/empleado';

// const sequelize = new Sequelize('AppCorte3', 'seminario', 'primercorte', {
//   host: 'ec2-3-16-13-188.us-east-2.compute.amazonaws.com:5432',
//   dialect: 'postgres',
// });

const sequelize = new Sequelize('postgres://seminario:primercorte@ec2-18-222-155-133.us-east-2.compute.amazonaws.com:5432/AppCorte3');

const User = UserModel(sequelize, Sequelize);
const Servicio = ServicioModel(sequelize, Sequelize);
const TipoServicio = TipoServicioModel(sequelize, Sequelize);
const Empleado = EmpleadoModel(sequelize, Sequelize);

sequelize.sync().then(() => {
  // eslint-disable-next-line no-console
  console.log('Users db and user table have been created');
});

module.exports = {
  User: User,
  Servicio: Servicio,
  TipoServicio: TipoServicio,
  Empleado: Empleado
};
